const { eventbusService } = require("../services");
const catchAsync = require("../utils/catchAsync");

const resisterNewEvent = catchAsync(async (req, res, next) => {
  const { type, data } = req.body;

  const event = {
    type,
    data,
  };
  //Received Event
  if (event) {
    console.log(`Event-bus service: Received event --> ${event.type}`);
  }

  //Process Received Event
  await eventbusService.dispatchEvent(event);
  res.status(200).send({ status: "OK" });
});

module.exports = {
  resisterNewEvent,
};
