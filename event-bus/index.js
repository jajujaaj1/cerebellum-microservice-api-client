const express = require("express");
const cors = require("cors");
const routes = require("./routes/index");

const connectDB = require("./config/db");
require("dotenv").config();

const app = express();

var corsOptions = {
  origin: ["http://localhost:8001"],
  optionsSuccessStatus: 200, // For legacy browser support
};
app.use(cors(corsOptions));

//Connect to Database
console.log(process.env.PORT);
connectDB();

// middleware
app.use(express.json());
app.use(express.urlencoded());

app.get("/", (req, res) => {
  res.send(`Event-Bus Service: Up and running on port ${port}`);
});

app.use("/v1", routes);
const port = process.env.PORT || 8000;

app.listen(port, () => {
  console.log(`Event-Bus Service: Up and running on port ${port}`);
});
