const express = require("express");

const eventRoutes = require("./v1/event.route");

const router = express.Router();

router.use("/events", eventRoutes);
module.exports = router;
