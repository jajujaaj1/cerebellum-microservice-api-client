const express = require("express");

const router = express.Router();
const eventController = require("../../controllers/event.controller");

router.route("/register").post(eventController.resisterNewEvent);

module.exports = router;
