const axios = require("axios");

const dispatchEvent = async (event) => {
  try {
    //Sending Event to all services
    //To --> UserService
    axios.post(process.env.USER_SERVICE_URI + "/event", event);
    //To --> OrderService
    axios.post(process.env.ORDER_SERVICE_URI + "/event", event);
    //To --> ItemService
    axios.post(process.env.ITEM_SERVICE_URI + "/event", event);

    console.log(
      `Event-bus service: Dispatched event to all services --> ${event.type}`
    );
  } catch (error) {
    console.log(error);
  }
};
module.exports = {
  dispatchEvent,
};
