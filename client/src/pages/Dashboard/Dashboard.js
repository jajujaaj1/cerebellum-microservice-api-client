import React, { useState } from "react";
import {
  Paper,
  makeStyles,
  Grid,
  Typography,
  Button,
  Box,
} from "@material-ui/core";

import ItemList from "../../components/ItemList";

const useStyles = makeStyles((theme) => ({
  // Top Right  Bottom Left
  paper: {
    margin: theme.spacing(2, 3, 1, 3),
    padding: theme.spacing(1, 5, 1, 5),
    borderRadius: 9,
  },
  logoutBtn: {},
  checkoutBtn: {
    margin: theme.spacing(0, 1, 0, 0),
  },
  listOfItems: {
    margin: theme.spacing(2, 0, 1, 0),
  },
}));

export default function Weather() {
  const classes = useStyles();
  return (
    <>
      <ItemList />
    </>
  );
}
