import React, { useState, useEffect } from "react";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import {
  Paper,
  makeStyles,
  Grid,
  TextField,
  Typography,
  Button,
} from "@material-ui/core";

import PageHeader from "../../components/PageHeader";
import CloudOutlinedIcon from "@material-ui/icons/CloudOutlined";
import WbSunnyIcon from "@material-ui/icons/WbSunny";

import { getCurrentWeatherOfCity } from "../../services/WeatherService";

const useStyles = makeStyles((theme) => ({
  // Top Right  Bottom Left
  paper: {
    margin: theme.spacing(5, 3, 0, 3),
    padding: theme.spacing(6, 5, 6, 5),
    borderRadius: 21,
  },
  cityName: {
    margin: theme.spacing(0, 5, 0, 5),
    padding: theme.spacing(0, 0, 0, 0),
    maxWidth: "20em",
  },
  temp: {
    margin: theme.spacing(0, 0, 0, 0),
    padding: theme.spacing(5, 0, 0, 0),
    textAlign: "center",
  },
  tempText: {
    color: "white",
  },
  cityText: {
    color: "white",
    borderBottom: "1px white",
    "&:after": {
      // The MUI source seems to use this but it doesn't work
      borderBottom: "2px solid white",
      labelColor: "white",
    },
  },
  button: {
    margin: theme.spacing(7, 0, 0, 0),
    backgroundColor: "#3485ea",
  },
  icon: {
    color: "white",
    fontSize: 90,
  },
}));

export default function Weather() {
  const classes = useStyles();
  const [degree, setDegree] = useState(0);
  const [city, setCity] = useState("Melbourne");
  const [cityFound, setCityFound] = useState("Melbourne");
  const [country, setCountry] = useState("Australia");

  async function getCityWeather() {
    const result = await getCurrentWeatherOfCity(city);
    setDegree(result.temp);
    setCityFound(result.city);
    setCountry(result.country);
  }
  return (
    <>
      <PageHeader
        title="Weather APP"
        subTitle="To get weather of cities"
        icon={<AccountBalanceIcon fontSize="large" />}
      />

      <Paper
        elevation={12}
        className={classes.paper}
        style={{
          "--color-1": "#00d4ff",
          "--color-2": "#090e7c",
          background: `
              linear-gradient(
                170deg,
                var(--color-1),
                var(--color-2) 90%
              )
            `,
        }}
      >
        <Grid container alignItems="center" justify="center" direction="row">
          <Grid
            item
            xs={12}
            sm={12}
            md={12}
            lg={12}
            xl={12}
            className={classes.cityName}
          >
            <TextField
              fullWidth
              label="City name"
              type="text"
              InputProps={{
                className: classes.cityText,
              }}
              value={city}
              onChange={(e) => setCity(e.target.value)}
            />
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            md={12}
            lg={12}
            xl={12}
            className={classes.temp}
          >
            {degree <= 18 ? (
              <CloudOutlinedIcon className={classes.icon} />
            ) : (
              <WbSunnyIcon className={classes.icon} />
            )}

            <Typography variant="h1" className={classes.tempText}>
              {degree}&deg;c
            </Typography>
            <Typography variant="h6" className={classes.tempText}>
              {cityFound} {country}
            </Typography>
          </Grid>
        </Grid>
      </Paper>
      <Grid container direction="column" alignItems="center" justify="center">
        <Grid item s={12} sm={12} md={12} lg={12} xl={12}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={getCityWeather}
          >
            Show Temp
          </Button>
        </Grid>
      </Grid>
    </>
  );
}
