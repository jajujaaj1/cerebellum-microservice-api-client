import React, { useState } from "react";
import {
  Paper,
  makeStyles,
  Grid,
  Button,
  TextField,
  Typography,
  Link,
} from "@material-ui/core";
import { Redirect } from "react-router";

const useStyles = makeStyles((theme) => ({
  gridItem: {
    // Top Right  Bottom Left
    margin: theme.spacing(10, 3, 0, 3),
    padding: theme.spacing(0, 10, 10, 10),
  },
  mainPaper: {
    // Top Right  Bottom Left
    margin: theme.spacing(10, 3, 0, 3),
    padding: theme.spacing(5, 8, 5, 8),
    borderRadius: 15,
  },
  loginText: {
    textAlign: "center",
    margin: theme.spacing(2, 0, 2, 0),
  },
  emailText: {
    margin: theme.spacing(0, 0, 2, 0),
  },
  loginBtn: {
    justify: "center",
    margin: theme.spacing(3, 0, 1, 0),
    padding: theme.spacing(1, 5, 1, 5),
  },
}));

export default function Login() {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");

  function loginRequest() {}

  return (
    <>
      <Grid container direction="column" alignItems="center" justify="center">
        <Grid item xs={12} sm={12} md={12} lg={12} className={classes.gridItem}>
          <Paper elevation={8} className={classes.mainPaper}>
            <Typography variant="h3" className={classes.loginText}>
              Login
            </Typography>
            <form onSubmit={loginRequest}>
              <TextField
                fullWidth
                label="Email"
                type="text"
                className={classes.emailText}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                fullWidth
                label="Password"
                type="password"
                value={pass}
                onChange={(e) => setPass(e.target.value)}
              />
              <Button
                variant="contained"
                color="primary"
                className={classes.loginBtn}
                onClick={loginRequest}
              >
                Login
              </Button>
            </form>
            <Button
              onClick={() => {
                console.log("sdfsdf");
                <Link to="/register" />;
              }}
            >
              Register as new user
            </Button>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
}
