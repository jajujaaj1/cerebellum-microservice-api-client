import React, { useState, useContext } from "react";
import { UserContext } from "../../UserContext";
import {
  Paper,
  makeStyles,
  Grid,
  Button,
  TextField,
  Typography,
} from "@material-ui/core";
import { signUpUser } from "../../services/SignUpService";
import { Redirect } from "react-router";

const useStyles = makeStyles((theme) => ({
  gridItem: {
    // Top Right  Bottom Left
    margin: theme.spacing(10, 3, 0, 3),
    padding: theme.spacing(0, 10, 10, 10),
  },
  mainPaper: {
    // Top Right  Bottom Left
    margin: theme.spacing(10, 3, 0, 3),
    padding: theme.spacing(5, 8, 5, 8),
    borderRadius: 15,
  },
  registerText: {
    textAlign: "center",
    margin: theme.spacing(2, 0, 3, 0),
  },
  text: {
    margin: theme.spacing(0, 0, 1, 0),
  },

  loginBtn: {
    justify: "center",
    margin: theme.spacing(3, 0, 1, 0),
    padding: theme.spacing(1, 5, 1, 5),
  },
}));

export default function SignUp() {
  const { setUser } = useContext(UserContext);
  const classes = useStyles();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");

  async function signUpRequest() {
    let body = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: pass,
    };
    body = JSON.stringify(body);
    const response = await signUpUser(body);
    const userData = await response.data;
    const userObj = {
      id: userData._id,
      firstName: userData.firstName,
      lastName: userData.lastName,
      password: userData.password,
    };
    setUser(userObj);
    localStorage.setItem("user", JSON.stringify(userObj));
    <Redirect to="/" />;
  }

  return (
    <>
      <Grid container direction="column" alignItems="center" justify="center">
        <Grid item xs={12} sm={12} md={7} lg={6} className={classes.gridItem}>
          <Paper elevation={8} className={classes.mainPaper}>
            <Typography variant="h3" className={classes.registerText}>
              Register
            </Typography>
            <form onSubmit={signUpRequest}>
              <TextField
                fullWidth
                label="First name"
                type="text"
                className={classes.text}
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
              <TextField
                fullWidth
                label="Last name"
                type="text"
                className={classes.text}
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
              <TextField
                fullWidth
                label="Email"
                type="text"
                className={classes.text}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                fullWidth
                label="Password"
                type="password"
                value={pass}
                onChange={(e) => setPass(e.target.value)}
              />
              <Button
                variant="contained"
                color="primary"
                className={classes.loginBtn}
                onClick={signUpRequest}
              >
                Login
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </>
  );
}
