import React, { useState } from "react";
import PeopleOutlineTwoToneIcon from "@material-ui/icons/PeopleOutlineTwoTone";
import { Paper, makeStyles, Grid, Button } from "@material-ui/core";

import PageHeader from "../../components/PageHeader";
import { getEmployee } from "../../services/EmployeeService";
const useStyles = makeStyles((theme) => ({
  pageContent: {
    margin: theme.spacing(0, 3, 0, 3),
    padding: theme.spacing(1, 0, 0, 1),
  },
  pageButton: {
    // Top Right  Bottom Left
    margin: theme.spacing(0, 3, 0, 3),
    padding: theme.spacing(0, 0, 2, 0),
  },
}));

export default function Employees() {
  const classes = useStyles();
  const [id, setId] = useState(1);
  const [data, setData] = useState([]);

  async function handleNewEmployeeRequest() {
    const userData = await getEmployee();
    setId(id + 1);
    setData([
      ...data,
      {
        _id: id,
        firstName: userData.name.first,
      },
    ]);
  }
  return (
    <>
      <PageHeader
        title="New Employee"
        subTitle="Form design with validation"
        icon={<PeopleOutlineTwoToneIcon fontSize="large" />}
      />

      <Grid container direction="column" alignItems="center" justify="center">
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={3}
          className={classes.pageButton}
        >
          <Paper elevation={3}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleNewEmployeeRequest}
            >
              Get New Employee
            </Button>
          </Paper>
        </Grid>
      </Grid>

      <Grid container>
        {data.map((item, index) => {
          return (
            <Grid item key={item._id} xs={12} sm={12} md={3} lg={3}>
              <Paper elevation={3} className={classes.pageContent}>
                <p>
                  {item.title} {item.firstName} {item.lastName}{" "}
                </p>
              </Paper>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}
