// import React from "react";
// import { makeStyles } from "@material-ui/core/styles";
// import AppBar from "@material-ui/core/AppBar";
// import Toolbar from "@material-ui/core/Toolbar";
// import Typography from "@material-ui/core/Typography";
// import Button from "@material-ui/core/Button";
// import IconButton from "@material-ui/core/IconButton";
// import MenuIcon from "@material-ui/icons/Menu";
// import CssBaseline from "@material-ui/core/CssBaseline";
// import clsx from "clsx";
// import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
// import Divider from "@material-ui/core/Divider";
// import Drawer from "@material-ui/core/Drawer";
// import List from "@material-ui/core/List";
// import { mainListItems, secondaryListItems } from "./listItems";
// import { useState } from "react";
// import { Redirect } from "react-router-dom";
// const drawerWidth = 240;

// const useStyles = makeStyles((theme) => ({
//   root: {
//     flexGrow: 1,
//     backgroundColor: "red",
//   },
//   menuButton: {
//     marginRight: theme.spacing(2),
//   },
//   title: {
//     flexGrow: 1,
//     color: "#ef233c",
//   },
//   appBar: {
//     backgroundColor: "#343a40",
//   },
//   button: {
//     color: "#d90429",
//   },
//   menuIcon: {
//     color: "#d90429",
//   },
//   toolbar: {
//     paddingRight: 24, // keep right padding when drawer closed
//   },
//   appBar: {
//     background: "#2E3B55",
//     zIndex: theme.zIndex.drawer + 1,
//     transition: theme.transitions.create(["width", "margin"], {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//   },
//   appBarShift: {
//     marginLeft: drawerWidth,
//     width: `calc(100% - ${drawerWidth}px)`,
//     transition: theme.transitions.create(["width", "margin"], {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.enteringScreen,
//     }),
//   },
//   menuButton: {
//     marginRight: 36,
//   },
//   menuButtonHidden: {
//     display: "none",
//   },
//   toolbarIcon: {
//     display: "flex",
//     alignItems: "center",
//     justifyContent: "flex-end",
//     padding: "0 8px",
//     ...theme.mixins.toolbar,
//   },
//   drawerPaper: {
//     position: "relative",
//     whiteSpace: "nowrap",
//     width: drawerWidth,
//     transition: theme.transitions.create("width", {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.enteringScreen,
//     }),
//   },
//   drawerPaperClose: {
//     overflowX: "hidden",
//     transition: theme.transitions.create("width", {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//     width: theme.spacing(7),
//     [theme.breakpoints.up("sm")]: {
//       width: theme.spacing(9),
//     },
//   },
//   content: {
//     flexGrow: 1,
//     height: "100vh",
//     overflow: "auto",
//   },
//   appBarSpacer: theme.mixins.toolbar,
//   container: {
//     paddingTop: theme.spacing(4),
//     paddingBottom: theme.spacing(4),
//   },
// }));

// function Header(props) {
//   const classes = useStyles();
//   const [open, setOpen] = useState(false);
//   function handleDrawer() {
//     if (open) setOpen(false);
//     else setOpen(true);
//   }
//   return (
//     <header className={classes.root}>
//       <CssBaseline />
//       <AppBar position="static" className={classes.appBar}>
//         <Toolbar>
//           {props.authState === "Logout" ? (
//             <IconButton
//               edge="start"
//               className={classes.menuButton}
//               color="inherit"
//               aria-label="menu"
//             >
//               <MenuIcon className={classes.menuIcon} />
//             </IconButton>
//           ) : (
//             <></>
//           )}
//           <Typography variant="h6" className={classes.title}>
//             {props.title}
//           </Typography>
//           <Button
//             color="inherit"
//             variant="outlined"
//             className={classes.button}
//             onClick={(e) => {
//               <Redirect to="/login" />;
//               console.log("done");
//             }}
//           >
//             {props.authState}
//           </Button>
//         </Toolbar>
//       </AppBar>
//       {props.authState === "Logout" ? (
//         <Drawer
//           variant="permanent"
//           classes={{
//             paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
//           }}
//           open={open}
//         >
//           <div className={classes.toolbarIcon}>
//             <IconButton onClick={handleDrawer}>
//               <ChevronLeftIcon />
//             </IconButton>
//           </div>
//           <Divider />
//           <List>{mainListItems}</List>
//           <Divider />
//           <List>{secondaryListItems}</List>
//         </Drawer>
//       ) : (
//         <></>
//       )}
//     </header>
//   );
// }

// export default Header;
