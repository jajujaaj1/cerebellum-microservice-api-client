import { useState, useMemo, useEffect } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";

import {
  makeStyles,
  CssBaseline,
  createMuiTheme,
  ThemeProvider,
} from "@material-ui/core";

import { UserContext } from "./UserContext";
import SideMenu from "./components/SideMenu";
import SignUp from "./pages/Authentication/SignUp";
import Login from "./pages/Authentication/Login";
import Header from "./components/Header";
import Dashboard from "./pages/Dashboard/Dashboard";
import Orders from "./components/Order";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#333996",
      light: "#3c44b126",
    },
    secondary: {
      main: "#f83245",
      light: "#f8324526",
    },
    background: {
      default: "#f4f5fd",
    },
  },
  overrides: {
    MuiAppBar: {
      root: {
        transform: "translateZ(0)",
      },
    },
  },
  props: {
    MuiIconButton: {
      disableRipple: true,
    },
  },
});

const useStyles = makeStyles({
  appMain: {
    paddingTop: "70px",
    //paddingLeft: "50px",
  },
});

function App() {
  const [user, setUser] = useState(null);
  const [order, setOrder] = useState(null);

  const providerValue = useMemo(
    () => ({
      user,
      setUser,
      order,
      setOrder,
    }),
    [user, order]
  );

  useEffect(() => {
    console.log("local storage user: ", localStorage.getItem("user"));
    setUser(localStorage.getItem("user"));
    setOrder([]);
  }, [user]);

  return (
    <>
      <Router>
        <ThemeProvider theme={theme}>
          <UserContext.Provider value={providerValue}>
            {user ? (
              <Switch>
                <Route path="/orders">
                  <Header />
                  <Orders />
                </Route>
                <Route path="/" exact>
                  <Header />
                  <Dashboard />
                </Route>
              </Switch>
            ) : (
              <>
                <Route path="/register">
                  <SignUp />
                </Route>
                <Route path="/login" exact>
                  <Login />
                </Route>
              </>
            )}
          </UserContext.Provider>
        </ThemeProvider>
        <CssBaseline />
      </Router>
    </>
  );
}

export default App;

// url: 'https://randomuser.me/api/'

{
  /* <SideMenu />
        <div className={classes.appMain}>
          <Weather />
        </div> */
}
