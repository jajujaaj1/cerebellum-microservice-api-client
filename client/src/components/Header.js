import React, { useState } from "react";
import {
  Paper,
  makeStyles,
  Grid,
  Typography,
  Button,
  Box,
} from "@material-ui/core";
import DashboardIcon from "@material-ui/icons/Dashboard";
import { Link, Redirect } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  // Top Right  Bottom Left
  paper: {
    margin: theme.spacing(2, 3, 1, 3),
    padding: theme.spacing(1, 5, 1, 5),
    borderRadius: 9,
  },
  logoutBtn: {},
  checkoutBtn: {
    margin: theme.spacing(0, 1, 0, 0),
  },
  listOfItems: {
    margin: theme.spacing(2, 0, 1, 0),
  },
  userName: {
    margin: theme.spacing(0, 0, 0, 2),
  },
}));

export default function Weather() {
  const classes = useStyles();

  function handleClick() {
    //Redirect to Order page
    <Redirect to="/orders" />;
  }
  return (
    <>
      <Paper elevation={12} className={classes.paper}>
        <Grid container alignItems="center" justify="center" direction="row">
          <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
            <Box display="flex" flexDirection="row" bgcolor="background.paper">
              <Box>
                <Box>
                  <Link to="/">
                    <DashboardIcon />
                  </Link>
                </Box>
              </Box>
              <Box className={classes.userName}>
                <Typography variant="h6" className={classes.tempText}>
                  Muhammad Junaid
                </Typography>
              </Box>
            </Box>
          </Grid>

          <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
            <Box
              display="flex"
              flexDirection="row-reverse"
              m={1}
              bgcolor="background.paper"
            >
              <Box className={classes.logoutBtn}>
                <Button variant="contained" color="secondary">
                  Logout
                </Button>
              </Box>
              <Box className={classes.checkoutBtn}>
                <Link to="/orders">
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleClick}
                  >
                    Cart
                  </Button>
                </Link>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Paper>
    </>
  );
}
