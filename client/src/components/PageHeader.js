import React from "react";
import { Paper, Card, Typography, makeStyles, Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#fdfdff",
  },
  pageHeader: {
    padding: theme.spacing(2),
    display: "flex",
    marginBottom: theme.spacing(2),
  },
  pageIcon: {
    display: "inline-block",
    padding: theme.spacing(),
    color: "#3c44b1",
  },
  pageTitle: {
    paddingLeft: theme.spacing(4),
    "& .MuiTypography-subtitle2": {
      opacity: "0.6",
    },
  },
  grid_Content: {
    margin: theme.spacing(1, 3, 0, 3),
    padding: theme.spacing(0),
  },
}));

export default function PageHeader(props) {
  const classes = useStyles();
  const { title, subTitle, icon } = props;
  return (
    <Grid container>
      <Grid item xs={12} sm={12} className={classes.grid_Content}>
        <Paper elevation={3} square className={classes.root}>
          <div className={classes.pageHeader}>
            <Card className={classes.pageIcon}>{icon}</Card>
            <div className={classes.pageTitle}>
              <Typography variant="h6" component="div">
                {title}
              </Typography>
              <Typography variant="subtitle2" component="div">
                {subTitle}
              </Typography>
            </div>
          </div>
        </Paper>
      </Grid>
    </Grid>
  );
}
