import React, { useState } from "react";
import { Paper, makeStyles, Grid, Typography, Button } from "@material-ui/core";
import OrderList from "../container/OrderList";
const useStyles = makeStyles((theme) => ({
  // Top Right  Bottom Left
  paper: {
    margin: theme.spacing(2, 3, 1, 3),
    padding: theme.spacing(1, 2, 1, 1),
    borderRadius: 9,
  },
  orderList: {
    margin: theme.spacing(2, 3, 1, 2),
  },
}));

export default function Weather() {
  const classes = useStyles();

  return (
    <>
      <Paper elevation={12} className={classes.paper}>
        <Grid container alignItems="center" justify="center" direction="row">
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <Typography variant="h6" className={classes.orderList}>
              Order List
            </Typography>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}></Grid>
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <OrderList />
          </Grid>
        </Grid>
      </Paper>
    </>
  );
}
