import React, { useState, useEffect, useContext } from "react";
import { makeStyles, Grid } from "@material-ui/core";
import { UserContext } from "../UserContext";
import Item from "../container/Item";
import { getAllItems } from "../services/ItemService";

const useStyles = makeStyles((theme) => ({
  // Top Right  Bottom Left
  gridItem: {
    margin: theme.spacing(2, 3, 1, 3),
    padding: theme.spacing(0, 0, 0, 0),
  },
}));

export default function Weather() {
  const classes = useStyles();
  const [items, setItems] = useState([]);

  const { order, setOrder } = useContext(UserContext);

  useEffect(() => {
    async function fetchData() {
      const response = await getAllItems();
      if (response) {
        setItems(await response.data);
      }
    }
    fetchData();
  }, []);

  return (
    <>
      <Grid container justify="center" align="center" direction="row">
        {items.map((item) => {
          return (
            <Grid
              item
              xs={3}
              sm={3}
              md={3}
              lg={3}
              xl={3}
              className={classes.gridItem}
              key={item._id}
            >
              <Item
                key={item._id}
                value={item}
                order={order}
                setOrder={setOrder}
              />
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}
