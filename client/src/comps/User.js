import { Container, Grid, makeStyles, Typography } from "@material-ui/core";

const users = [
  {
    id: "1",
    name: "_Mine",
  },
  {
    id: "2",
    name: "_Mine",
  },
  {
    id: "3",
    name: "_Mine",
  },
  {
    id: "4",
    name: "_Mine",
  },
];

const useStyles = makeStyles((theme) => ({
  grid_item: {
    backgroundColor: "red",
  },
}));
export default function User() {
  const classes = useStyles();
  return (
    <>
      <Container>
        <Grid container>
          <Grid
            item
            xs={12}
            sm={12}
            md={12}
            lg={12}
            className={classes.grid_item}
          >
            <Typography variant="h3">USERS</Typography>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}
