import Header from "./Header";
import User from "./User";
export default function Index() {
  return (
    <div>
      <Header component={<User />} />
    </div>
  );
}
