const axios = require("axios");
let URL = "";

export async function getAllItems() {
  if (process.env.NODE_ENV === "development") {
    URL = process.env.REACT_APP_ITEM_SERVICE_URI;
  } else URL = process.env.REACT_APP_AXIOS_BASE_URL_PRODUCTION;

  try {
    const response = await axios.get(URL, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.status === 200) {
      return response;
    } else {
      console.log("Unable to get list of Items", response);
    }
  } catch (error) {
    console.error(error);
  }
}

export async function getItemByIds(body) {
  if (process.env.NODE_ENV === "development") {
    URL = process.env.REACT_APP_ITEM_SERVICE_URI + "/customlist";
  } else URL = process.env.REACT_APP_AXIOS_BASE_URL_PRODUCTION;

  try {
    const response = await axios.post(URL, body, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.status === 200) {
      return response;
    } else {
      console.log("Unable to get list of Items", response);
    }
  } catch (error) {
    console.error(error);
  }
}

export async function postOrder(body) {
  if (process.env.NODE_ENV === "development") {
    URL = process.env.REACT_APP_ORDER_SERVICE_URI + "/placeorder";
  } else URL = process.env.REACT_APP_AXIOS_BASE_URL_PRODUCTION;

  try {
    const response = await axios.post(URL, body, {
      headers: {
        "Content-Type": "application/json",
        "bearer-token": localStorage.getItem("user"),
      },
    });
    if (response.status === 200) {
      return response;
    } else {
      console.log("Unable to place order", response);
    }
  } catch (error) {
    console.error(error);
  }
}
