const axios = require("axios");
let URL = "";

export async function getEmployee() {
  if (process.env.REACT_ENV === "DEVELOPMENT") {
    URL = process.env.REACT_APP_AXIOS_BASE_URL_DEVELOPMENT;
  } else URL = process.env.REACT_APP_AXIOS_BASE_URL_PRODUCTION;

  try {
    const response = await axios.get(URL);
    const userData = await response.data.results[0];
    return userData;
  } catch (error) {
    console.error(error);
  }
}
