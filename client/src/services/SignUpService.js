const axios = require("axios");
let URL = "";

export async function signUpUser(body) {
  if (process.env.NODE_ENV === "development") {
    URL = process.env.REACT_APP_USER_SERVICE_URI;
  } else URL = process.env.REACT_APP_AXIOS_BASE_URL_PRODUCTION;

  try {
    console.log(process.env.NODE_ENV, URL, body);
    const response = await axios.post(URL, body, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.status === 201) {
      return response;
    } else {
      console.log("Unable to signup");
    }
  } catch (error) {
    console.error(error);
  }
}
