const axios = require("axios");

export async function getCurrentWeatherOfCity(city) {
  let URL = `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=99907cfcd7efb59582c393976d3b5703`;
  try {
    const response = await axios.get(URL);
    const temp = Math.round((await response.data.main.temp) - 273.15);
    const city = await response.data.name;
    const country = await response.data.sys.country;
    return {
      temp,
      city,
      country,
    };
  } catch (error) {
    console.error(error);
  }
}
