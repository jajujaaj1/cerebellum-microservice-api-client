import { useContext, useEffect, useState } from "react";
import { UserContext } from "../UserContext";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { getItemByIds, postOrder } from "../services/ItemService";
import { useSnackbar } from "notistack";
import Slide from "@material-ui/core/Slide";

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 500,
  },
  confirmOrder: {
    margin: theme.spacing(0, 3, 1, 2),
  },
}));

export default function OrderList() {
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const [rows, setRows] = useState([]);
  function createData(name, quantity) {
    return { name, quantity };
  }

  const { order, user } = useContext(UserContext);

  useEffect(() => {
    async function getOrderItemDetails() {
      const response = await getItemByIds(order);
      checkAvailabilityOfOrderingItems(response.data);
    }
    getOrderItemDetails();
  }, []);

  function checkAvailabilityOfOrderingItems(currenItems) {
    order.map((itemId) => {
      currenItems.map((avaItem) => {
        if (avaItem._id === itemId) {
          return (avaItem.__v = avaItem.__v + 1);
        }
      });
    });

    currenItems.map((item) => {
      if (item.__v > item.quantity) {
        enqueueSnackbar(
          `Item ${item.name} only ${item.quantity} left in stock `,
          {
            anchorOrigin: {
              vertical: "bottom",
              horizontal: "right",
            },
            variant: "error",
            TransitionComponent: Slide,
          }
        );
        enqueueSnackbar(
          `Cart updated with ${item.quantity} items for "${item.name}" `,
          {
            anchorOrigin: {
              vertical: "bottom",
              horizontal: "right",
            },
            variant: "info",
            TransitionComponent: Slide,
          }
        );
        return (item.__v = item.quantity);
      }
    });
    setRows(currenItems);
  }
  async function placeOrder() {
    const response = await postOrder(rows);
  }
  return (
    <>
      <Button
        variant="contained"
        style={{ backgroundColor: "green", color: "white" }}
        className={classes.confirmOrder}
        onClick={placeOrder}
      >
        Confirm Order
      </Button>
      <TableContainer component={Paper} className={classes.table}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Article Name</TableCell>
              <TableCell align="right">Quantity</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row._id}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.__v}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
