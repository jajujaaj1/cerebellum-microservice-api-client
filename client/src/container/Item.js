import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useSnackbar } from "notistack";
import Slide from "@material-ui/core/Slide";

const useStyles = makeStyles({
  root: {
    //maxWidth: 345,
    borderRadius: 15,
  },
  media: {
    height: 100,
  },
});

export default function Item(props) {
  const classes = useStyles();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const { _id, name, quantity, url } = props.value;
  const order = props.order;
  const setOrder = props.setOrder;

  const handleClick = () => {
    enqueueSnackbar(`Item ${name} added to cart`, {
      anchorOrigin: {
        vertical: "bottom",
        horizontal: "right",
      },
      variant: "success",
      TransitionComponent: Slide,
    });
    setOrder([...order, _id]);
  };

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia className={classes.media} image={url} title="Item" />
        <CardContent>
          <Typography gutterBottom variant="h6" component="h1">
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            Quantity: {quantity}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={handleClick}>
          Order
        </Button>
      </CardActions>
    </Card>
  );
}
