const express = require("express");

const router = express.Router();
const itemController = require("../../controllers/item.controller");

router.route("/").post(itemController.createItem);
router.route("/customlist").post(itemController.getItemsByIds);
router.route("/").get(itemController.getAllItems);
router.route("/event").post(itemController.receiveEvent);

module.exports = router;
