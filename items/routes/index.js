const express = require("express");

const itemRoutes = require("./v1/item.route");

const router = express.Router();

router.use("/items", itemRoutes);
module.exports = router;
