const express = require("express");
const cors = require("cors");
const routes = require("./routes/index");

const connectDB = require("./config/db");
require("dotenv").config();

const app = express();
app.use(cors());

//Connect to Database
connectDB();

// middleware
app.use(express.json());
//app.use(express.urlencoded());

app.get("/", (req, res) => {
  res.send(`UserService: Up and running on port ${port}`);
});

app.use("/v1", routes);
const port = process.env.PORT || 8002;

app.listen(port, () => {
  console.log(`UserService: Up and running on port ${port}`);
});
