const { itemService } = require("../services");

const catchAsync = require("../utils/catchAsync");

const createItem = catchAsync(async (req, res) => {
  const item = await itemService.createItem(req.body);
  if (item._id) {
    await itemService.createEvent(item);
    console.log("Item service: Dispated event --> ItemCreated");
    return res.status(201).send(item);
  } else {
    return res.status(400).send(item);
  }
});

const getItemsByIds = catchAsync(async (req, res) => {
  const result = await itemService.getItemsByIds(req.body);
  res.status(200).send(result);
});

const getAllItems = catchAsync(async (req, res) => {
  const result = await itemService.queryItems();
  res.send(result);
});

const receiveEvent = catchAsync(async (req, res) => {
  const result = await itemService.receiveEvent(req.body);
  if (result) res.status(200).send({ status: "ok", message: "Updated" });
});

module.exports = {
  receiveEvent,
  createItem,
  getAllItems,
  getItemsByIds,
};
