const axios = require("axios");
const { Item } = require("../models");

const queryItems = async () => {
  try {
    const items = await Item.find();
    return items;
  } catch (error) {
    return error;
  }
};

const createItem = async (itemBody) => {
  try {
    const item = await Item.create(itemBody);
    return item;
  } catch (error) {
    return error;
  }
};

const deleteAll = async () => {
  try {
  } catch (error) {
    return error;
  }
};

const createEvent = async (data) => {
  try {
    const result = await axios.post(
      process.env.EVENT_BUS_SERVICE_URI + "/register",
      {
        type: "ItemCreated",
        data,
      }
    );
    return result;
  } catch (error) {
    console.log(error);
  }
};

const getItemsByIds = async (ids) => {
  try {
    const items = await Item.find({ _id: { $in: ids } });
    return items;
  } catch (error) {
    return error;
  }
};

const receiveEvent = async (body) => {
  const { type, data } = body;
  console.log(`Item Service: Received event --> ${type}`);

  if (type === "OrderCreated") {
    let ids = [];
    data.map((i) => {
      ids = [...ids, i.item];
    });
    const foundItems = await Item.find({ _id: { $in: ids } });

    data.map((i) => {
      foundItems.map((item) => {
        if (item._id.toString() === i.item) {
          return (item.quantity = item.quantity - i.quantity);
        }
      });
    });

    foundItems.map((item) => {
      Item.findOneAndUpdate(
        { _id: item._id },
        item,
        function (error, result) {}
      );
    });
  }
  return 1;
};

module.exports = {
  createItem,
  queryItems,
  deleteAll,
  createEvent,
  receiveEvent,
  getItemsByIds,
};
