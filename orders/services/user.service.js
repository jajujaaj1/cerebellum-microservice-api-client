const { User } = require("../models");

const queryUsers = async () => {
  const users = await User.find();
  return users;
};

const createUser = async (userBody) => {
  try {
    await User.create(userBody);
  } catch (error) {
    return error;
  }
};

module.exports = {
  createUser,
};
