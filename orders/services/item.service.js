const { Item } = require("../models");

const createItem = async (itemBody) => {
  try {
    const item = await Item.create(itemBody);
    return item;
  } catch (error) {
    return error;
  }
};

module.exports = {
  createItem,
};
