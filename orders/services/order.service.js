const { createUser } = require("./user.service");
const { createItem } = require("./item.service");
const { Order } = require("../models");
const { Item } = require("../models");
const axios = require("axios");

const receiveEvent = async (body) => {
  const { type, data } = body;
  console.log(`Order Service: Received event --> ${type}`);

  switch (type) {
    case "UserCreated":
      await createUser(data);
      break;
    case "ItemCreated":
      await createItem(data);
      break;
    case "OrderCreated":
      await updateItems(data);
      break;
    default:
    // code block
  }
  return 1;
};

const updateItems = async (data) => {
  let ids = [];
  data.map((i) => {
    ids = [...ids, i.item];
  });
  const foundItems = await Item.find({ _id: { $in: ids } });

  data.map((i) => {
    foundItems.map((item) => {
      if (item._id.toString() === i.item) {
        return (item.quantity = item.quantity - i.quantity);
      }
    });
  });

  foundItems.map((item) => {
    Item.findOneAndUpdate({ _id: item._id }, item, function (error, result) {});
  });
};

const placeOrder = async (req) => {
  const data = req.body;
  const user = JSON.parse(req.get("bearer-token"));
  let body = [];
  data.map((item) => {
    body = [
      ...body,
      {
        item: item._id,
        quantity: item.__v,
      },
    ];
  });
  const updatedBody = {
    user: user.id,
    orderList: body,
  };
  const response = await Order.create(updatedBody);
  await createEvent(body);
  return response;
};

const createEvent = async (data) => {
  try {
    const result = await axios.post(
      process.env.EVENT_BUS_SERVICE_URI + "/register",
      {
        type: "OrderCreated",
        data,
      }
    );
    return result;
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  receiveEvent,
  placeOrder,
  createEvent,
};
