const { orderService } = require("../services");

const catchAsync = require("../utils/catchAsync");

const receiveEvent = catchAsync(async (req, res) => {
  const result = await orderService.receiveEvent(req.body);
  res.status(200).send({ status: "ok", message: "//TODO" });
});

const placeOrder = catchAsync(async (req, res) => {
  const response = await orderService.placeOrder(req);
  res.status(200).send(response);
});

module.exports = {
  receiveEvent,
  placeOrder,
};
