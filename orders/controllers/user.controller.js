const { userService } = require("../services");

const catchAsync = require("../utils/catchAsync");

const getAllUsers = catchAsync(async (req, res) => {
  const result = await userService.queryUsers();
  res.send(result);
});

const createUser = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  if (user._id) {
    const eventResult = await userService.createEvent(req.body);

    if (eventResult.status === 200) {
      console.log("User service: Dispated event --> UserCreated");
    }
    return res.status(201).send(user);
  } else {
    return res.status(400).send(user);
  }
});

const deleteAll = catchAsync(async (req, res) => {
  const result = await userService.deleteAll();
  res.status(200).send({ status: "ok", message: "Deleted all users", result });
});

const receiveEvent = catchAsync(async (req, res) => {
  const result = await userService.receiveEvent(req.body);
  res.status(200).send({ status: "ok", message: "//TODO" });
});

module.exports = {
  createUser,
  getAllUsers,
  deleteAll,
  receiveEvent,
};
