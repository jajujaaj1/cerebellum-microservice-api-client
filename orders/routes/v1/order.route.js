const express = require("express");

const router = express.Router();
const orderController = require("../../controllers/order.controller");

router.route("/event").post(orderController.receiveEvent);
router.route("/placeorder").post(orderController.placeOrder);

module.exports = router;
