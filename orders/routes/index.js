const express = require("express");

const orderRoutes = require("./v1/order.route");

const router = express.Router();

router.use("/orders", orderRoutes);
module.exports = router;
