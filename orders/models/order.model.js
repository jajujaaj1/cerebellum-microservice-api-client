const mongoose = require("mongoose");

const orderSchema = mongoose.Schema({
  user: String,
  orderList: [{ item: String, quantity: Number }],
});

/**
 * @typedef User
 */
const Order = mongoose.model("Order", orderSchema);

module.exports = Order;
