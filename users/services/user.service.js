const { User } = require("../models");
const axios = require("axios");

const queryUsers = async () => {
  const users = await User.find();
  return users;
};

const createUser = async (userBody) => {
  try {
    const user = await User.create(userBody);
    return user;
  } catch (error) {
    return error;
  }
};

const deleteAll = async () => {
  try {
    const result = await User.deleteMany();
    return result;
  } catch (error) {
    return error;
  }
};

const createEvent = async (data) => {
  try {
    const result = await axios.post(
      process.env.EVENT_BUS_SERVICE_URI + "/register",
      {
        type: "UserCreated",
        data,
      }
    );
    return result;
  } catch (error) {
    console.log(error);
  }
};

const receiveEvent = async (body) => {
  const { type, data } = body;
  console.log(`User Service: Received event --> ${type}`);
  return 1;
};

module.exports = {
  createUser,
  queryUsers,
  deleteAll,
  createEvent,
  receiveEvent,
};
