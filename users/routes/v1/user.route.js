const express = require("express");

const router = express.Router();
const userController = require("../../controllers/user.controller");

router.route("/").get(userController.getAllUsers);
router.route("/").post(userController.createUser);
router.route("/deleteAll").post(userController.deleteAll);
router.route("/event").post(userController.receiveEvent);

module.exports = router;
