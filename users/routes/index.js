const express = require("express");

const userRoutes = require("./v1/user.route");

const router = express.Router();

router.use("/users", userRoutes);
module.exports = router;
